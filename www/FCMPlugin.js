var exec = require('cordova/exec');
alert("bb1");
function FCMPlugin() { 
alert("bb2");
	console.log("FCMPlugin.js: is created");
}
alert("bb4");
// SUBSCRIBE TO TOPIC //
FCMPlugin.prototype.subscribeToTopic = function( topic, success, error ){
	exec(success, error, "FCMPlugin", 'subscribeToTopic', [topic]);
}
// UNSUBSCRIBE FROM TOPIC //
FCMPlugin.prototype.unsubscribeFromTopic = function( topic, success, error ){
	exec(success, error, "FCMPlugin", 'unsubscribeFromTopic', [topic]);
}
// NOTIFICATION CALLBACK //
FCMPlugin.prototype.onNotification = function( callback, success, error ){
	FCMPlugin.prototype.onNotificationReceived = callback;
	exec(success, error, "FCMPlugin", 'registerNotification',[]);
}
// TOKEN REFRESH CALLBACK //
FCMPlugin.prototype.onTokenRefresh = function( callback ){
	FCMPlugin.prototype.onTokenRefreshReceived = callback;
}
// GET TOKEN //
FCMPlugin.prototype.getToken = function( success, error ){
	exec(success, error, "FCMPlugin", 'getToken', []);
}

// DEFAULT NOTIFICATION CALLBACK //
FCMPlugin.prototype.onNotificationReceived = function(payload){
	console.log("Received push notification")
	console.log(payload)
}
// DEFAULT TOKEN REFRESH CALLBACK //
FCMPlugin.prototype.onTokenRefreshReceived = function(token){
	console.log("Received token refresh")
	console.log(token)
}

// Analytics Plugin

FCMPlugin.prototype.logEvent = function(eventName, eventParams, success, error){
  exec(success, error, 'FCMPlugin', 'logEvent', [eventName, eventParams || {}]);
}

FCMPlugin.prototype.setUserId = function(userId, success, error){
  exec(success, error, 'FCMPlugin', 'setUserId', [userId]);
}

FCMPlugin.prototype.setUserProperty = function(name, value, success, error){
  exec(success, error, 'FCMPlugin', 'setUserProperty', [name, value]);
}
alert("bb5");
// FIRE READY //
exec(function(result){ console.log("FCMPlugin NG Ready OK") }, function(result){ console.log("FCMPlugin Ready ERROR") }, "FCMPlugin",'ready',[]);
alert("bb6");
var fcmPlugin = new FCMPlugin();
alert("bb7");
module.exports = fcmPlugin;
alert("bb8");